﻿Console.WriteLine("Enter a sentence: ");

string input = Console.ReadLine();

string[] sentence = input.Split(' ');

Boolean duplicates = false;

for (int i = 0; i < sentence.Length; i++)
{
    for (int j = i+1; j < sentence.Length; j++)
    {
        if (sentence[i].Equals(sentence[j]))
        {
            duplicates = true;
        }
    }
}

Console.WriteLine("Duplicates in your sentence: " + duplicates);